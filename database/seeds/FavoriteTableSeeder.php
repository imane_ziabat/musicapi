<?php

use Illuminate\Database\Seeder;

class FavoriteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('favorites')->delete();

        $faker = \Faker\Factory::create();
        
        $musicIds = DB::table('musics')->pluck('id');
        $userIds = DB::table('users')->pluck('id');

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            \App\Models\Favorite::create([
                'user_id' => $faker->randomElement($userIds),
                'music_id' => $faker->randomElement($musicIds)
            ]);
        }
    }
}
