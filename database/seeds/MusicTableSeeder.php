<?php

use Illuminate\Database\Seeder;

class MusicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('musics')->delete();

        $faker = \Faker\Factory::create();
        $categoryIds = DB::table('categories')->pluck('id');

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            \App\Models\Music::create([
                'name' => $faker->sentence,
                'description' => $faker->paragraph,
                'filePath' => $faker->url,
                'category_id' =>  $faker->randomElement($categoryIds)
            ]);
        }
    }
}
