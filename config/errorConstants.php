<?php

return [
    'errors' => [
        'not_found' => 'not_found',
        'failed_operation' => 'failed_operation'
    ]
];
