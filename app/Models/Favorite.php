<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['user_id', 'music_id'];

    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function music() {
        return $this->belongsTo(Music::class);
    }

    public function getByUserId($userId) {
        return self::where('user_id', $userId)->get();
    }

    public function setFavorite($user_id, $music_id) {
        return self::firstOrCreate(
                        [
                            'user_id' => $user_id,
                            'music_id' => $music_id
                        ]
        );
    }

    public function getFavorite($user_id, $music_id) {
        return self::where(
                        [
                            'user_id' => $user_id,
                            'music_id' => $music_id
                        ]
                )->first();
    }
}
