<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $fillable = ['name', 'description'];
    
    public function musics()
    {
      return $this->hasMany('App\Models\Music');
    }
    
    public static function getList($perPage,$page) {
        return self::paginate($perPage,"*","Page",$page);    
    }
    
}
