<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Music extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $fillable = ['name', 'description','filePath','category_id'];
    
    public function category()
    {
      return $this->belongsTo(Category::class,'category_id');
    }
    
    public function favorites()
    {
      return $this->hasMany(Favorite::class);
    }
}
