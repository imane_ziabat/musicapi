<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Http\Resources\FavoriteCollection;
use App\Http\Resources\FavoriteResource;
use Illuminate\Http\Request;

class FavoriteController extends Controller {

    private $model;

    public function __construct() {
        if (!$this->model)
            $this->model = new Favorite();
    }

    /**
     * Display a user's favorite list.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaginatedListByUserId(Request $request) {
        $request->validate([
            'perPage' => 'integer',
            'page' => 'integer',
        ]);

        $entity = $this->model->getByUserId($this->user()->id);

        $collection = new FavoriteCollection($entity);

        return response_success($collection);
    }

    /**
     * Set a music as favorite.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request) {
        $request->validate([
            'music_id' => 'required|integer'
        ]);

        $music = \App\Models\Music::where(
                        [
                            'id' => $request->music_id
                        ]
                )->first();

        if (!$music) {
            return response_error(constants('error.not_found'), 1);
        }
        
        $favorite = $this->model->getFavorite($this->user()->id, $request->music_id);
        
        $entity = $this->model->setFavorite($this->user()->id, $request->music_id);

        $collection = new FavoriteResource($entity);

        return response_success($collection);
    }

    /**
     * remove a music from user's favorite list.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request) {
        $request->validate([
            'music_id' => 'required|integer'
        ]);

        $favorite = $this->model->getFavorite($this->user()->id, $request->music_id);

        $deleted = false;

        if (!$favorite) {
            return response_error(constants('error.not_found'), 1);
        }

        $deleted = $favorite->delete();

        if (!$deleted) {
            return response_error(constants('error.failed_operation'), 1);
        }

        return response_success();
    }
}
