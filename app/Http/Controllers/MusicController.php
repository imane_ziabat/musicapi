<?php

namespace App\Http\Controllers;

use App\Models\Music;
use App\Http\Resources\MusicCollection;
use Illuminate\Http\Request;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaginatedListByCategoryId(Request $request)
    {
        $request->validate([
            'category_id' => 'required|integer',
            'perPage' => 'integer',
            'page' => 'integer',
        ]);
        
        $musics = Music::where('category_id', $request->category_id)->get();
        
        return new MusicCollection($musics);
    }
}
