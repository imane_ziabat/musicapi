<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\CategoryCollection;

class CategoryController extends Controller {

    /**
     * Display a list of categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaginatedList(Request $request) {
        $request->validate([
            'perPage' => 'integer', // If null, return all
            'page' => 'integer',
        ]);

        $categories = Category::getList($request->perPage, $request->page);

        $categoryCollection = new CategoryCollection($categories);
        return response_success($categoryCollection);
    }

}
