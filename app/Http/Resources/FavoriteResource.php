<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FavoriteResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'user_id' => $this->user_id,
            'music_id' => $this->music_id,
            'title' => $this->name,
            'description' => $this->description,
            'category' => $this->category,
            'user' => $this->user,
            'music' => $this->music,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at
        ];
    }

}
