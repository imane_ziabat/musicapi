<?php

if (!function_exists("response_error")) {

    /**
     * @param string $message Error message
     * @param string $code Optional custom error code
     * @param string | array $data Optional data
     * @param int $status HTTP status code
     * @param array $extraHeaders
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    function response_error($message, $code = null, $data = null, $status = 500) {
        $response = [
            "status" => "error",
            "errorCode" => $code,
            "errorMessage" => $message
        ];
        !is_null($code) && $response['code'] = $code;
        !is_null($data) && $response['data'] = $data;
        return response()->json($response, $status);
    }

}

if (!function_exists("response_success")) {

    /**
     * @param array | Illuminate\Database\Eloquent\Model $data
     * @param int $status HTTP status code
     * @param array $extraHeaders
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    function response_success($data = []) {
        $response = [
            "status" => "success",
            "errorCode" => "",
            "errorMessage" => "",
            "data" => $data
        ];
        return response()->json($response, 200);
    }

}

if ( ! function_exists('constants'))
{
    function constants($key)
    {
       return config('constants.' . $key);
    }
}
