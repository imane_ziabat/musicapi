<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});


Route::get('/category', 'CategoryController@getPaginatedList')->middleware('auth:api');
Route::get('/music', 'MusicController@getPaginatedListByCategoryId')->middleware('auth:api');

Route::get('/myFavorites', 'FavoriteController@getPaginatedListByUserId')->middleware('auth:api');
Route::post('/myFavorites/add', 'FavoriteController@save')->middleware('auth:api');
Route::post('/myFavorites/remove', 'FavoriteController@remove')->middleware('auth:api');
